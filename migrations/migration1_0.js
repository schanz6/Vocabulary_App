module.exports = {
  up: function(migration, DataTypes, done) {
    
    //Creatse dishes table
migration.createTable(
        'definitions',
        {
          id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
         
           definition: {
            type: DataTypes.TEXT, 
            allowNull: false
          },
          attempt: {
            type: DataTypes.INTEGER
          },
          correct: {
            type: DataTypes.INTEGER
          },
          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          }

        });

     migration.createTable(
        'words',
        {
          id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          word: {
            type: DataTypes.TEXT, 
            allowNull: false
          },
          definitionId: {
            type: DataTypes.INTEGER, 
            allowNull: false
          },
          meaning: {
            type: DataTypes.TEXT, 
            allowNull: true
          },
          attempt: {
            type: DataTypes.INTEGER
          },
          correct: {
            type: DataTypes.INTEGER
          },
          createdAt: {
            type: DataTypes.DATE
          },
          updatedAt: {
            type: DataTypes.DATE
          }
        });

    
    done();
   
   
  },
  down: function(migration, DataTypes, done) {
    
     migration.dropTable('definitions');
     migration.dropTable('words');
    

    done();
  }
};

