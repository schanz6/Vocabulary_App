# Vocabulary App


## Description 
The purpose of the Vocabulary App is to help you learn GRE vocabulary words. It currently has two modules flashcards and matching game. The flashcards game allows you to review vocabulary words and keeps track of your answers. The matching game shows you four words (can be changed) and you must select the two words that mean the same thing. 

## Setup
1. Download project
2. Create a MySQL database and add its connection information to config.json in the config folder
3. In base directory run the following terminal commands 
	1. npm init
	2. npm run migrate  (creates tables) 
	3. npm run seed  (fills the database with test data) 
	4. npm start
4. View in browser with path http//http://127.0.0.1:4000/


