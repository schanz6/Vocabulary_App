
var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var db = require('./database/database');

//include routes 
var words = require('./routes/words');
var definitions = require('./routes/definitions');

var app = express();

app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api/words', words);
app.use('/api/definitions', definitions);


app.use('/Metro_Visualization', express.static(__dirname +'/../../Metro_Visualization'));
app.use( express.static(__dirname +'/../../Profile/client'));
app.use('/Vocabulary_App',express.static(__dirname + '/../client'));
//app.use(dexpress.static(__dirname + '/../client'));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
 
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;