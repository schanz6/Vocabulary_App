var express = require('express');
var router = express.Router();
var db    = require(__dirname + '/../database/database');

/* GET all definitions*/
router.get('/', function(req, res, next) {
  
  db.definitionsDB.findAll({include: [
        { model: db.wordsDB, as: 'Words'}
    ]})
    .then(function(retDefinitions) {
          res.send(retDefinitions)
    })
    .catch(function(err) {
          res.status(500).send();
    })

});

/* GET definition by ID */
router.get('/:id', function(req, res, next) {
  
   //get passed ID param 
  var id = req.params.id ;
  
  db.definitionsDB.findAll({where: { id : id }, include: [
        { model: db.wordsDB, as: 'Words'}
    ]})
    .then(function(retDefinitions) {
          res.send(retDefinitions)
    })
    .catch(function(err) {
          res.status(500).send();
    })

  
});


/* POST new definitions */
router.post('/', function(req, res, next) {

  var newDefinition = req.body; 
 
   db.definitionsDB.create( newDefinition )
    .then(function(retDefinitions) {
          res.send(retDefinitions);

    })
    .catch(function(err) {
          res.status(500).send();
    })

  
});

/* PUT update Definitions based on ID */
router.put('/:id', function(req, res, next) {
  
  var id = req.params.id ;
  var UpdateDefinitions = req.body; 


  db.definitionsDB.find({ where: {id: id} })
    .then(function(retDef) {
    
      // if the record exists in the db
      if (retDef) { 
          
          retDef.updateAttributes(UpdateDefinitions)
            .then(function() {

                res.send();

            })
            .catch(function(err) {
              res.status(500).send();
            })
      }
      else{
        res.status(404).send();
      }
    })
    .catch(function(err) {
            res.status(500).send();
    })

});



/* DELETE definition based on ID */
router.delete('/:id', function(req, res, next) {
 
  var id = req.params.id ;
 
  db.definitionsDB.destroy({where: {id: id}})
    .then(function() {
          res.send({ id: id });
    })
    .catch(function(err) {
          res.status(500).send();
    })

});



module.exports = router;