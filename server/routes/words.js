var express = require('express');
var router = express.Router();
var db    = require(__dirname + '/../database/database');

/* GET all words with a meaning */
router.get('/', function(req, res, next) {
  
  db.wordsDB.findAll({ where : {meaning:{ $ne : null} }})
    .then(function(retReviews) {
          res.send(retReviews)
        })
    

});

/* GET word by ID */
router.get('/:id', function(req, res, next) {
   
  var id = req.params.id ;
  
  db.wordsDB.findAll({where: { id: id}})
    .then(function(retReviews) {
          res.send(retReviews)
    })
    .catch(function(err) {
            res.status(500).send();
      })
  

});

/* POST new word */
router.post('/', function(req, res, next) {

  var newWord = req.body; 
 
   db.wordsDB.create( newWord  )
    .then(function(retWords) {
          res.send(retWords);

    })
    .catch(function(err) {
          res.status(500).send();
    })

  
});




/* POST new words with same definition id */
router.post('/:id', function(req, res, next) {

  var newWords = req.body; 
  var id = req.params.id ;
   //res.send(newWords+" "+id);

 newWords.words.forEach(function(element)
 {
    var newWord = {
      word : element,
      definitionId: id,
      createdAt : "2015-08-22T15:11:11.000Z"

    }

   db.wordsDB.create(newWord)
    .then(function(retReview) {
        //res.send(retReview);

    })
    .catch(function(err) {
        res.status(500).send();
      })
  })

 res.send();


});

/* PUT update word based on ID */
router.put('/update/:id', function(req, res, next) {
 
  
   var updateWords = req.body; 
   var id = req.params.id ;


  db.wordsDB.find({ where: {id: id} })
    .then(function(retWord) {
    
      // if the record exists in the db
      if (retWord) { 
          
          retWord.updateAttributes(updateWords)
            .then(function() {

                res.send();

            })
            .catch(function(err) {
              res.status(500).send();
            })
      }
      else{
        res.status(404).send();
      }
    })
    .catch(function(err) {
            res.status(500).send();
    })


  
});



/* PUT Delete words based on defintion id -(angular delete you can not pass array) */
router.put('/:id', function(req, res, next) {
 
  
   var deleteWords = req.body; 
   var id = req.params.id ;

  deleteWords.words.forEach(function(element)
  {
    db.wordsDB.destroy({where: {word: element, definitionId : id}})
    .then(function() {
         
    })
    .catch(function(err) {
          res.status(500).send();
    })
});

    res.send();

});


module.exports = router;