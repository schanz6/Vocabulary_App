var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(__dirname + '/../../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);


/* 
In order to run this you need to download mysql for your computer
https://www.youtube.com/watch?v=NN_O-DXyp50

Make sure 
DB name : GreProgram
Username : root
password ; '' (nothing)

*/


//
// create objects of tables 
//

 var wordsDB     = sequelize.define('words', {
     
     word:  Sequelize.TEXT,  
     definitionId: Sequelize.INTEGER,
     meaning : Sequelize.TEXT,
     correct: Sequelize.INTEGER,
     attempt: Sequelize.INTEGER  
     
 })


 var definitionsDB     = sequelize.define('definitions', {
      
       definition : Sequelize.TEXT,
       correct: Sequelize.INTEGER,
       attempt: Sequelize.INTEGER   
        
 })



 definitionsDB.hasMany( wordsDB , {foreignKey: 'definitionId', as: 'Words'});

module.exports = {
   sequelize : sequelize,
   wordsDB : wordsDB,
   definitionsDB : definitionsDB
  
};