var db = require('./database');



//Add data to tables
//Create Login
db.definitionsDB.create({ 
    id: 1,
    definition:'First',
    correct: 0,
    attempt: 0  
})

db.definitionsDB.create({ 
    id: 2,
    definition:'Second',
    correct: 0,
    attempt: 0  
})

db.definitionsDB.create({ 
    id: 3,
    definition:'Third',
    correct: 0,
    attempt: 0  
})

//Create words

db.wordsDB.create({ 
    id: 1,
    word: 'One',  
    definitionId : 1,
    correct: 0,
    attempt: 0  
})

db.wordsDB.create({ 
    id: 2,
    word: 'Uno',  
    definitionId : 1,
    correct: 0,
    attempt: 0  
})

db.wordsDB.create({ 
    id: 3,
    word: '1',  
    definitionId : 1,
    correct: 0,
    attempt: 0  
})

db.wordsDB.create({ 
    id: 4,
    word: 'Two',  
    definitionId : 2,
    correct: 0,
    attempt: 0  
})


db.wordsDB.create({ 
    id: 5,
    word: 'Duo',  
    definitionId : 2,
    correct: 0,
    attempt: 0  
})

db.wordsDB.create({ 
    id: 6,
    word: '2',  
    definitionId : 2,
    correct: 0,
    attempt: 0  
})

db.wordsDB.create({ 
    id: 7,
    word: 'three',  
    definitionId : 3,
    correct: 0,
    attempt: 0  
})


db.wordsDB.create({ 
    id: 8,
    word: 'tres',  
    definitionId : 3,
    correct: 0,
    attempt: 0  
})

db.wordsDB.create({ 
    id: 9,
    word: '3',  
    definitionId : 3,
    correct: 0,
    attempt: 0  
})

//Words with definitions 

db.wordsDB.create({ 
    id: 10,
    word: "hello",
    meaning: 'used as a greeting.:To begin a telephone conversation.',  
    definitionId : -1,
    correct: 0,
    attempt: 0  
})

db.wordsDB.create({ 
    id: 11,
    word: "world",
    meaning: 'the earth, together with all of its countries, peoples, and natural features.',  
    definitionId : -1,
    correct: 0,
    attempt: 0  
})


      

