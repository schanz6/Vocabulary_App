var app = require('./app.js');
var logger = require('morgan');
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + '/../config/config.json')[env];
var db = require('./database/database');

var server;
var port = config.portNumber;

startServer(port);

module.exports = {
	start: startServer,
	stop: stopServer
};

// TODO: use actual logger

function startServer(port) {
	if (server && running) {
		console.log('Server already running on port ' + port);
		return;
	}

	server = app.listen(port);

	server.on('listening', function() {
		console.log('Server listening on ' + server.address().port);
		running = true;
	});

	server.on('close', function() {
		console.log('Exiting server');
		running = false;
	});

	server.on('error', function(err) {
		console.error('Fatal server error', err);
		running = false;
	});
}

function stopServer() {
	if (server && running) {
		server.close();
	}
}