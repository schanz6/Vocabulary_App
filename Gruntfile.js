module.exports = function(grunt){

	grunt.initConfig({
	  concat: {
	    js: {
	      src: [],
	      dest: 'build/js/scripts.js',
	    },
	     css: {
	      src: [],
	      dest: 'build/css/styles.js',
	    },
	  },
	  watch: {
		server: {
	      files: 'server/**/*.js', //any file in any folder ending with js
	      tasks: ['jshint:server'],
	      
	    },
	    clientJS: {
	      files: 'client/app/**/*.js', //any file in any folder ending with js
	      tasks: ['jshint:clientJS'],
	      
	    },
	  },
	  jshint: {
	  	
	      options: {
	        asi: true, //ignore semicolons missing error
	      },
    		
    	server: ['Gruntfile.js', 'server/**/*.js'],
    	clientJS: ['client/app/**/*.js'], 
  	  },

	});

	grunt.registerTask('default', ['concat','watch']);

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
};
