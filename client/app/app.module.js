(function () { 
	angular.module('app', [
		'app.common',
		'app.home',
		'app.manage',
		'app.flashcard',
		'app.matching'

		]);

})();