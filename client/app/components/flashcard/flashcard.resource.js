(function () { 
	
	angular
		.module('app.flashcard')
		.factory('flashcardsResource', flashcardsResource);

	flashcardsResource.$inject = ['$resource','common'];

	function flashcardsResource($resource, common)
	{
		var resourceUrl = common.apiBaseUrl;

		return $resource(resourceUrl,
		{
			id: '@id'

		},
		{
			getWords:
			{
				url: resourceUrl + 'api/words',
				method: 'GET',
				isArray : true

			},
			update:
			{
				url: resourceUrl + 'api/words/update/:id',
				method: 'PUT'
				
			}

		});

	}



})();