(function () { 
	
	angular
		.module('app.flashcard')
		.controller('Flashcards', Flashcards);

	Flashcards.$inject = [ 'flashcardsResource','words','common'];

	function Flashcards( flashcardsResource, words, common)
	{
		
		var vm = this;
		vm.message = '';
		vm.words = words; 
		vm.selectedWord = words[0];
		vm.meanings = [];
		vm.isViewDefinition = false; 
		vm.index = 0;

		
		vm.getNewWord = function()
		{
			vm.isViewDefinition = false;  
			vm.index = Math.floor(Math.random()*vm.words.length);
			vm.selectedWord = vm.words[vm.index]; 
			vm.meanings = vm.selectedWord.meaning.split(":");
			vm.meanings.shift();

		};

		vm.getNewWord();

		vm.viewDefinition = function()
		{
			vm.isViewDefinition = true; 
			
		};

		vm.correct = function()
		{
			var update = { id : vm.selectedWord.id, correct : vm.selectedWord.correct+1, attempt: vm.selectedWord.attempt+1 };
			flashcardsResource.update({ id: vm.selectedWord.id}, update);
			vm.words[vm.index].correct = vm.words[vm.index].correct+1;
			vm.words[vm.index].attempt = vm.words[vm.index].attempt+1;
			vm.getNewWord();
		};

		vm.wrong = function()
		{
			var update = { id : vm.selectedWord.id, correct : vm.selectedWord.correct, attempt: vm.selectedWord.attempt+1 };
			flashcardsResource.update({ id: vm.selectedWord.id}, update );
			vm.words[vm.index].attempt = vm.words[vm.index].attempt+1;
			vm.getNewWord();
		};

		
	}
	
})();