(function () { 
	
	angular
		.module('app.flashcard', ["ui.router",'ngResource'])
		.config(routeConfig);

	routeConfig.$inject = ['$stateProvider','$urlRouterProvider'];

	function routeConfig( $stateProvider , $urlRouterProvider)
	{
		
		$urlRouterProvider.otherwise("/");

		$stateProvider
			.state("flashcard",
			{
				url: "/flashcard",
				templateUrl: "app/components/flashcard/flashcard.html",
				controller: "Flashcards as vm",
				resolve : {
					flashcardsResource: "flashcardsResource",
					
					words: function(flashcardsResource, $stateParams)
					{
						
						return flashcardsResource.getWords().$promise;
					}
					
				}
				
			});

	}



})();