(function () { 
	
	angular
		.module('app.manage')
		.controller('Manages', Manages);

	Manages.$inject = [ 'managesResource','matching','common'];

	function Manages( managesResource, matching, common)
	{
		var vm = this;
		
		 vm.matching = matching;
		 vm.message = ''; 
		 vm.newDefinition = '';
		 vm.defSelected = vm.matching[0];
		 vm.list = [];
		 vm.originalList = [];
		 vm.newWord = "";
		 vm.flashcardMessage = "";


		 vm.defChange = function()
		 {
		 	var list = [];
            
		 	for(var i = 0; i< vm.defSelected.Words.length; i++)
		 	{
		 		list.push(vm.defSelected.Words[i].word);
		 	}
		 

		 	vm.list = list;
		 	vm.originalList = list; 

		 };
		 
		 //loads edit word box  on open
		 if (typeof vm.defSelected  != "undefined") {
  			 vm.defChange();
		 }

		 

		 vm.submitMatching = function()
		 {	

		 	var remove = vm.originalList;
		 	var add = vm.list;
		 	//compare original list with changed list 
		 	for(var i = remove.length-1; i>= 0; i--)
		 	{
		 		var index = add.indexOf(remove[i]);

		 		if (index > -1) 
		 		{
   					 add.splice(index, 1);
   					 remove.splice(i,1);
				}

		 	}
		 	// removes words no longer in list
		 	if(remove.length > 0)
		 	{
		 		managesResource.deleteWords({ id: vm.defSelected.id}, {words : remove });
		 	}

		 	if(add.length > 0)
		 	{
		 		managesResource.newWords({ id: vm.defSelected.id}, {words : add });
		 	}

		 	managesResource.getMatching(function(data)
		 	{
	 			vm.matching = data;
	 			vm.defSelected = vm.matching[0];
	 			vm.defChange();
	 			vm.newDefinition = '';

		 	});


		 }; 

		 vm.submitNewDefinition = function()
		 {
		 	managesResource.newDefinition({definition: vm.newDefinition});

		 	managesResource.getMatching(function(data)
		 	{
	 			vm.matching = data;
	 			vm.defSelected = vm.matching[vm.matching.length-1];
	 			vm.defChange();
	 			vm.newDefinition = '';

		 	});

		 };

		 vm.submitNewWord = function()
		 {
	 		managesResource.getDictionary({word:vm.newWord}, function(data)
	 		{
	 			var xml = common.reponceToXml(data);
				dt = xml.find('dt');
				

				if(dt.length > 0 )
				{
					var meaning = "";

					for(var i = 0; i < dt.length;i++)
					{
						meaning +=  dt[i].innerText;

					}

					var post = {
						word : vm.newWord,
						definitionId : -1,
						meaning : meaning
					}

					managesResource.newWord({},post);
					vm.flashcardMessage = vm.newWord+" added successfully";
					vm.newWord = "";
				}
				else
				{
					vm.flashcardMessage = "Definition not founed";
				}
	 			
	 		});

		 };

		
	}
	
})();