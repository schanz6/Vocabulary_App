(function () { 
	
	angular
		.module('app.manage', ["ui.router",'ngResource'])
		.config(routeConfig);

	routeConfig.$inject = ['$stateProvider','$urlRouterProvider'];

	function routeConfig( $stateProvider , $urlRouterProvider)
	{
		
		$urlRouterProvider.otherwise("/");

		$stateProvider
			.state("manage",
			{
				url: "/manage",
				templateUrl: "app/components/manage/manage.html",
				controller: "Manages as vm",
				resolve : {
					managesResource: "managesResource",
					
					matching: function(managesResource, $stateParams)
					{
						
						return managesResource.getMatching().$promise;
					}
					
				}
				
			});

	}



})();