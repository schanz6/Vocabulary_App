(function () { 
	
	angular
		.module('app.manage')
		.factory('managesResource', managesResource);

	managesResource.$inject = ['$resource','common'];

	function managesResource($resource, common)
	{
		var resourceUrl = common.apiBaseUrl;
		var dictionaryApi = common.dictionaryApi;
		var headers = {
				'Access-Control-Allow-Origin' : '*'
			};


		return $resource(resourceUrl,
		{
			id: '@id',
			word: '@word'

		},
		{
			getMatching:
			{
				url: resourceUrl + 'api/definitions',
				method: 'GET',
				isArray : true

			},
			getDictionary:
			{
				url : dictionaryApi,
				headers : headers,
				method : 'GET'
			},
			newWord:
			{
				url: resourceUrl+'api/words',
				method: 'POST'
			},
			newWords:
			{
				url: resourceUrl+'api/words/:id',
				method: 'POST'
			},
			deleteWords:
			{
				url: resourceUrl+'api/words/:id',
				method: 'Put'
			},

			newDefinition:
			{
				url: resourceUrl + 'api/definitions',
				method: 'POST'
			}

		});

	}



})();