(function () { 
	
	angular
		.module('app.home', ["ui.router",'ngResource'])
		.config(routeConfig);

	routeConfig.$inject = ['$stateProvider','$urlRouterProvider'];

	function routeConfig( $stateProvider , $urlRouterProvider)
	{
		
		$urlRouterProvider.otherwise("/");

		$stateProvider
			.state("home",
			{
				url: "/",
				templateUrl: "app/components/home/home.html",
				controller: "Homes as vm"
			});

	}



})();