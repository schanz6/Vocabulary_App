(function () { 
	
	angular
		.module('app.home')
		.factory("homeResource", homeResource);

	homeResource.$inject = ['$resource','common'];

	function homeResource($resource, common)
	{
		var resourceUrl = common.apiBaseUrl +'api/definitions/:id';

		return $resource(resourceUrl,
		{
			id: '@id'

		},
		{
			
		});

	}



})();