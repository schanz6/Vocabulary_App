(function () { 
	
	angular
		.module('app.matching')
		.controller('Matchings', Matchings);

	Matchings.$inject = ['matchingsResource','words','common'];

	function Matchings( matchingsResource, words ,common)
	{
		var vm = this;
		vm.message = ''; 
		vm.correct = true; 
		vm.definitions = words; 
		vm.choices = [];

	
		vm.getNew = function()
		{
			var listSize = 4;
			vm.correct = true; 

			var index = common.random(listSize,listSize);
			var defRandome = common.random(listSize-1,vm.definitions.length);
			vm.selectedDef = vm.definitions[defRandome[0]];

			var wordRandome = common.random(2,vm.selectedDef.Words.length);

			var list = new Array(listSize);
			vm.answer = [index[0], index[1]];

			list[index[0]] = vm.selectedDef.Words[wordRandome[0]];
			list[index[1]] = vm.selectedDef.Words[wordRandome[1]];

			for( var i = 2; i< listSize;i++)
			{
				var otherItem = vm.definitions[defRandome[i-1]];
				list[index[i]] = otherItem.Words[Math.floor(Math.random()*otherItem.Words.length)];
			}

			vm.choices = [];

			for(  i = 0; i< list.length;i++)
			{
				vm.choices.push({
					word : list[i],
					isSelected : false
				});
			}

		};

		vm.getNew(); 

		vm.submit = function()
		{
			var result = [];

			for( var i = 0; i< vm.choices.length;i++)
			{
				if(vm.choices[i].isSelected)
				{
					result.push(i); 
				}
			}
			
			if(result.length === 2)
			{
				result.sort();
				vm.answer.sort();

				//correct result
				if(result[0] === vm.answer[0] && result[1] === vm.answer[1])
				{
					vm.message = "CORRECT "+ vm.choices[result[0]].word.word+" and "+ vm.choices[result[1]].word.word+ " Both mean "+vm.selectedDef.definition;
					
					var correct = vm.selectedDef.correct;
					if(vm.correct) correct++; 

					var update = { id : vm.selectedDef.id, correct : correct, attempt: vm.selectedDef.attempt+1 };

					matchingsResource.update({ id: vm.selectedDef.id}, update );

					vm.getNew(); 
				}
				else
				{
					vm.message = "WRONG "+ vm.choices[result[0]].word.word+" and "+ vm.choices[result[1]].word.word+ " do not mean the samething" ;
					vm.correct = false;

				}

			}
			else
			{
				//error
				vm.message = "Two boxes must be selected"
			}
		}

		



	}



})();