(function () { 
	
	angular
		.module('app.matching')
		.factory("matchingsResource", matchingsResource);

	matchingsResource.$inject = ['$resource','common'];

	function matchingsResource($resource, common)
	{
		var resourceUrl = common.apiBaseUrl +'api/definitions/:id';

		return $resource(resourceUrl,
		{
			id: '@id'

		},
		{
			get:
			{
				method: 'GET',
				isArray : true

			},
			update:
			{
				method: 'PUT'

			}
		});

	}



})();