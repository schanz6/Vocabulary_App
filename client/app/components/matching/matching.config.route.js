(function () { 
	
	angular
		.module('app.matching', ["ui.router",'ngResource'])
		.config(routeConfig);

	routeConfig.$inject = ['$stateProvider','$urlRouterProvider'];

	function routeConfig( $stateProvider , $urlRouterProvider)
	{
		
		$urlRouterProvider.otherwise("/");

		$stateProvider
			.state("matching",
			{
				url: "/matching",
				templateUrl: "app/components/matching/matching.html",
				controller: "Matchings as vm",
				resolve : {
					matchingsResource: "matchingsResource",
					
					words : function(matchingsResource, $stateParams)
					{
						
						return matchingsResource.get().$promise;
					}
					
				}
			});

	}



})();