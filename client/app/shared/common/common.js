(function () { 
	
	angular
		.module('app.common')
		.factory('common',common);

	common.$inject = ['$resource'];

	function common($resource)
	{
		co = this;
		co.apiBaseUrl ="http://localhost:3000/";
		co.dictionaryApi = "http://www.dictionaryapi.com/api/v1/references/collegiate/xml/:word?key=bd785185-c54d-4a98-b005-9eab7bf411cb";
		
		//returns x amount of randome numbers between 0 and ceiling
		co.random = function(number, ceiling)
		{
			var arr = []
			while(arr.length < number)
			{
			  var randomnumber=Math.floor(Math.random()*ceiling);
			  var found=false;

			  for(var i=0;i<arr.length;i++)
			  {
				if(arr[i]==randomnumber)
				{
					found=true;
					break;
				}
			  }
			  
			  if(!found)
			  {
			  	arr[arr.length]=randomnumber;
			  }
			}

			return arr

		};

		//reads in responce and returns xml object 
		co.reponceToXml = function(data)
		{
			var index = 0;
 			var xmlString = "";

 			while(typeof data[index] !== "undefined" )
 			{
 				xmlString += data[index++];

 			}

 			xmlDoc = $.parseXML( xmlString );
 			xml = $( xmlString );

 			return  xml; 
		};

			
		return { 	
			apiBaseUrl: co.apiBaseUrl, 
			random : co.random, 
			dictionaryApi: co.dictionaryApi,
			reponceToXml : co.reponceToXml
		};


	}



})();